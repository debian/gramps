#!/usr/bin/make -f

# Turn on verbose mode.
export DH_VERBOSE=1
export DH_OPTIONS=-v

export PYBUILD_NAME=gramps
# export PYBUILD_INSTALL_ARGS_python3=--resourcepath=/usr/share --force

%:
	dh $@ --with python3 --buildsystem=pybuild

# Override auto_test to set HOME to find Gramps resources
override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	mkdir -p "$(CURDIR)/build/.gramps"
	HOME=$(CURDIR)/build \
	python3 setup.py test
endif

execute_before_dh_auto_build:
	@echo "blhc: ignore-line-regexp: ^.*Compiling\\s+po/\\S+\\.po\\s.*"
	@echo "blhc: ignore-line-regexp: ^.*Compiling\\s+data/\\S+\\.in\\s.*"

# Override of auto_install to remove information from package
override_dh_auto_install:
	dh_installchangelogs NEWS
	dh_auto_install
	# Remove install file as it is not needed by package users
	rm $(CURDIR)/debian/gramps/usr/share/doc/gramps/INSTALL
	# Remove NEWS file as it is now installed as the changelog
	rm $(CURDIR)/debian/gramps/usr/share/doc/gramps/NEWS
	#Remove license information in COPYING file & link to system license
	rm $(CURDIR)/debian/gramps/usr/share/doc/gramps/COPYING
	ln -s /usr/share/common-licenses/GPL-2 $(CURDIR)/debian/gramps/usr/share/doc/gramps/COPYING

# Fix clean target
override_dh_clean:
	rm -rf data/tips.xml
	rm -rf gramps/plugins/lib/holidays.xml
	rm -rf po/.intltool-merge-cache
	dh_clean

# Don't compress example files
override_dh_compress:
	dh_compress -X sample.ged -X data.gramps -X example.gramps
